﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
This experiment was created using PsychoPy3 Experiment Builder (v2023.2.3),
    on Februar 13, 2024, at 16:07
If you publish work using this script the most relevant publication is:

    Peirce J, Gray JR, Simpson S, MacAskill M, Höchenberger R, Sogo H, Kastman E, Lindeløv JK. (2019) 
        PsychoPy2: Experiments in behavior made easy Behav Res 51: 195. 
        https://doi.org/10.3758/s13428-018-01193-y

"""

import psychopy
psychopy.useVersion('2023.2.3')


# --- Import packages ---
from psychopy import locale_setup
from psychopy import prefs
from psychopy import plugins
plugins.activatePlugins()
prefs.hardware['audioLib'] = 'ptb'
prefs.hardware['audioLatencyMode'] = '3'
from psychopy import sound, gui, visual, core, data, event, logging, clock, colors, layout
from psychopy.tools import environmenttools
from psychopy.constants import (NOT_STARTED, STARTED, PLAYING, PAUSED,
                                STOPPED, FINISHED, PRESSED, RELEASED, FOREVER, priority)

import numpy as np  # whole numpy lib is available, prepend 'np.'
from numpy import (sin, cos, tan, log, log10, pi, average,
                   sqrt, std, deg2rad, rad2deg, linspace, asarray)
from numpy.random import random, randint, normal, shuffle, choice as randchoice
import os  # handy system and path functions
import sys  # to get file system encoding

from psychopy.hardware import keyboard

# --- Setup global variables (available in all functions) ---
# Ensure that relative paths start from the same directory as this script
_thisDir = os.path.dirname(os.path.abspath(__file__))
# Store info about the experiment session
psychopyVersion = '2023.2.3'
expName = 'crtt_exp'  # from the Builder filename that created this script
expInfo = {
    'Participant': '',
    'Age (in years)': '',
    'Handedness ': ["right", "left"],
    'Session': '',
    'Do you like this session?': ["yes", "no"],
    'output-path': '',
    'date': data.getDateStr(),  # add a simple timestamp
    'expName': expName,
    'psychopyVersion': psychopyVersion,
}


def showExpInfoDlg(expInfo):
    """
    Show participant info dialog.
    Parameters
    ==========
    expInfo : dict
        Information about this experiment, created by the `setupExpInfo` function.
    
    Returns
    ==========
    dict
        Information about this experiment.
    """
    # temporarily remove keys which the dialog doesn't need to show
    poppedKeys = {
        'date': expInfo.pop('date', data.getDateStr()),
        'expName': expInfo.pop('expName', expName),
        'psychopyVersion': expInfo.pop('psychopyVersion', psychopyVersion),
    }
    # show participant info dialog
    dlg = gui.DlgFromDict(dictionary=expInfo, sortKeys=False, title=expName)
    if dlg.OK == False:
        core.quit()  # user pressed cancel
    # restore hidden keys
    expInfo.update(poppedKeys)
    # return expInfo
    return expInfo


def setupData(expInfo, dataDir=None):
    """
    Make an ExperimentHandler to handle trials and saving.
    
    Parameters
    ==========
    expInfo : dict
        Information about this experiment, created by the `setupExpInfo` function.
    dataDir : Path, str or None
        Folder to save the data to, leave as None to create a folder in the current directory.    
    Returns
    ==========
    psychopy.data.ExperimentHandler
        Handler object for this experiment, contains the data to save and information about 
        where to save it to.
    """
    
    # data file name stem = absolute path + name; later add .psyexp, .csv, .log, etc
    if dataDir is None:
        dataDir = _thisDir
    filename = u'%s\%s_%s_%s_%s' % (expInfo['output-path'], expInfo['Participant'], expInfo['Session'], expName, expInfo['date'])
    # make sure filename is relative to dataDir
    if os.path.isabs(filename):
        dataDir = os.path.commonprefix([dataDir, filename])
        filename = os.path.relpath(filename, dataDir)
    
    # an ExperimentHandler isn't essential but helps with data saving
    thisExp = data.ExperimentHandler(
        name=expName, version='',
        extraInfo=expInfo, runtimeInfo=None,
        originPath='C:\\Users\\Kyuchukova\\Desktop\\NOWASchool\\choice_rtt\\code\\experiment\\crtt_exp_lastrun.py',
        savePickle=True, saveWideText=True,
        dataFileName=dataDir + os.sep + filename, sortColumns='time'
    )
    thisExp.setPriority('thisRow.t', priority.CRITICAL)
    thisExp.setPriority('expName', priority.LOW)
    # return experiment handler
    return thisExp


def setupLogging(filename):
    """
    Setup a log file and tell it what level to log at.
    
    Parameters
    ==========
    filename : str or pathlib.Path
        Filename to save log file and data files as, doesn't need an extension.
    
    Returns
    ==========
    psychopy.logging.LogFile
        Text stream to receive inputs from the logging system.
    """
    # this outputs to the screen, not a file
    logging.console.setLevel(logging.EXP)
    # save a log file for detail verbose info
    logFile = logging.LogFile(filename+'.log', level=logging.EXP)
    
    return logFile


def setupWindow(expInfo=None, win=None):
    """
    Setup the Window
    
    Parameters
    ==========
    expInfo : dict
        Information about this experiment, created by the `setupExpInfo` function.
    win : psychopy.visual.Window
        Window to setup - leave as None to create a new window.
    
    Returns
    ==========
    psychopy.visual.Window
        Window in which to run this experiment.
    """
    if win is None:
        # if not given a window to setup, make one
        win = visual.Window(
            size=[1536, 864], fullscr=True, screen=0,
            winType='pyglet', allowStencil=False,
            monitor='testMonitor', color=[0,0,0], colorSpace='rgb',
            backgroundImage='', backgroundFit='none',
            blendMode='avg', useFBO=True,
            units='height'
        )
        if expInfo is not None:
            # store frame rate of monitor if we can measure it
            expInfo['frameRate'] = win.getActualFrameRate()
    else:
        # if we have a window, just set the attributes which are safe to set
        win.color = [0,0,0]
        win.colorSpace = 'rgb'
        win.backgroundImage = ''
        win.backgroundFit = 'none'
        win.units = 'height'
    win.mouseVisible = False
    win.hideMessage()
    return win


def setupInputs(expInfo, thisExp, win):
    """
    Setup whatever inputs are available (mouse, keyboard, eyetracker, etc.)
    
    Parameters
    ==========
    expInfo : dict
        Information about this experiment, created by the `setupExpInfo` function.
    thisExp : psychopy.data.ExperimentHandler
        Handler object for this experiment, contains the data to save and information about 
        where to save it to.
    win : psychopy.visual.Window
        Window in which to run this experiment.
    Returns
    ==========
    dict
        Dictionary of input devices by name.
    """
    # --- Setup input devices ---
    inputs = {}
    ioConfig = {}
    ioSession = ioServer = eyetracker = None
    
    # create a default keyboard (e.g. to check for escape)
    defaultKeyboard = keyboard.Keyboard(backend='ptb')
    # return inputs dict
    return {
        'ioServer': ioServer,
        'defaultKeyboard': defaultKeyboard,
        'eyetracker': eyetracker,
    }

def pauseExperiment(thisExp, inputs=None, win=None, timers=[], playbackComponents=[]):
    """
    Pause this experiment, preventing the flow from advancing to the next routine until resumed.
    
    Parameters
    ==========
    thisExp : psychopy.data.ExperimentHandler
        Handler object for this experiment, contains the data to save and information about 
        where to save it to.
    inputs : dict
        Dictionary of input devices by name.
    win : psychopy.visual.Window
        Window for this experiment.
    timers : list, tuple
        List of timers to reset once pausing is finished.
    playbackComponents : list, tuple
        List of any components with a `pause` method which need to be paused.
    """
    # if we are not paused, do nothing
    if thisExp.status != PAUSED:
        return
    
    # pause any playback components
    for comp in playbackComponents:
        comp.pause()
    # prevent components from auto-drawing
    win.stashAutoDraw()
    # run a while loop while we wait to unpause
    while thisExp.status == PAUSED:
        # make sure we have a keyboard
        if inputs is None:
            inputs = {
                'defaultKeyboard': keyboard.Keyboard(backend='PsychToolbox')
            }
        # check for quit (typically the Esc key)
        if inputs['defaultKeyboard'].getKeys(keyList=['escape']):
            endExperiment(thisExp, win=win, inputs=inputs)
        # flip the screen
        win.flip()
    # if stop was requested while paused, quit
    if thisExp.status == FINISHED:
        endExperiment(thisExp, inputs=inputs, win=win)
    # resume any playback components
    for comp in playbackComponents:
        comp.play()
    # restore auto-drawn components
    win.retrieveAutoDraw()
    # reset any timers
    for timer in timers:
        timer.reset()


def run(expInfo, thisExp, win, inputs, globalClock=None, thisSession=None):
    """
    Run the experiment flow.
    
    Parameters
    ==========
    expInfo : dict
        Information about this experiment, created by the `setupExpInfo` function.
    thisExp : psychopy.data.ExperimentHandler
        Handler object for this experiment, contains the data to save and information about 
        where to save it to.
    psychopy.visual.Window
        Window in which to run this experiment.
    inputs : dict
        Dictionary of input devices by name.
    globalClock : psychopy.core.clock.Clock or None
        Clock to get global time from - supply None to make a new one.
    thisSession : psychopy.session.Session or None
        Handle of the Session object this experiment is being run from, if any.
    """
    # mark experiment as started
    thisExp.status = STARTED
    # make sure variables created by exec are available globally
    exec = environmenttools.setExecEnvironment(globals())
    # get device handles from dict of input devices
    ioServer = inputs['ioServer']
    defaultKeyboard = inputs['defaultKeyboard']
    eyetracker = inputs['eyetracker']
    # make sure we're running in the directory for this experiment
    os.chdir(_thisDir)
    # get filename from ExperimentHandler for convenience
    filename = thisExp.dataFileName
    frameTolerance = 0.001  # how close to onset before 'same' frame
    endExpNow = False  # flag for 'escape' or other condition => quit the exp
    # get frame duration from frame rate in expInfo
    if 'frameRate' in expInfo and expInfo['frameRate'] is not None:
        frameDur = 1.0 / round(expInfo['frameRate'])
    else:
        frameDur = 1.0 / 60.0  # could not measure, so guess
    
    # Start Code - component code to be run after the window creation
    
    # --- Initialize components for Routine "new_task" ---
    introduction = visual.TextStim(win=win, name='introduction',
        text='Welcome to my experiment!\n\nPlease press the space bar to continue.',
        font='Open Sans',
        pos=(0, 0), height=0.05, wrapWidth=None, ori=0.0, 
        color=[-1.0000, -0.2157, -1.0000], colorSpace='rgb', opacity=None, 
        languageStyle='LTR',
        depth=0.0);
    spacebar_introduction = keyboard.Keyboard()
    
    # --- Initialize components for Routine "instructions" ---
    text_instructions = visual.TextStim(win=win, name='text_instructions',
        text='In this task, you will make decisions as to which stimulus you have seen. There are three versions of the task. In the first, you have to decide which shape you have seen, in the second which image and the third which sound you have heard. Before each task, you will get set of specific instructions and short practice period. \n\nPlease press the space bar to continue.',
        font='Open Sans',
        pos=(0,0), height=0.05, wrapWidth=None, ori=0.0, 
        color=[1.0000, 0.2549, -0.0431], colorSpace='rgb', opacity=None, 
        languageStyle='LTR',
        depth=0.0);
    spacebar_intro = keyboard.Keyboard()
    
    # --- Initialize components for Routine "instruct_shape" ---
    instructions_shape = visual.TextStim(win=win, name='instructions_shape',
        text='In this task you will make a decision as to which shape you have seen.\n\nPress C or click cross for a cross. Press V or click square for a square. Press B or click plus for a plus.\n\nFirst, we will have a quick practice.\n\nPush space bar or click / touch one of the buttons to begin. ',
        font='Open Sans',
        pos=(0, 0), height=0.05, wrapWidth=None, ori=0.0, 
        color=[1.0000, 0.9451, 0.7255], colorSpace='rgb', opacity=None, 
        languageStyle='LTR',
        depth=0.0);
    instruct_space_bar = keyboard.Keyboard()
    image_square = visual.ImageStim(
        win=win,
        name='image_square', 
        image='C:/Users/Kyuchukova/Desktop/NOWASchool/choice_rtt/stimuli/shapes/response_square.jpg', mask=None, anchor='center',
        ori=0.0, pos=(0, -0.4), size=(0.2, 0.1),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=False, flipVert=False,
        texRes=128.0, interpolate=True, depth=-2.0)
    image_plus = visual.ImageStim(
        win=win,
        name='image_plus', 
        image='C:/Users/Kyuchukova/Desktop/NOWASchool/choice_rtt/stimuli/shapes/response_plus.jpg', mask=None, anchor='center',
        ori=0.0, pos=(0.25, -0.4), size=(0.2, 0.1),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=False, flipVert=False,
        texRes=128.0, interpolate=True, depth=-3.0)
    image_cross = visual.ImageStim(
        win=win,
        name='image_cross', 
        image='C:/Users/Kyuchukova/Desktop/NOWASchool/choice_rtt/stimuli/shapes/response_cross.jpg', mask=None, anchor='center',
        ori=0.0, pos=(-0.25, -0.4), size=(0.2, 0.1),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=False, flipVert=False,
        texRes=128.0, interpolate=True, depth=-4.0)
    
    # --- Initialize components for Routine "trial" ---
    image_square_trial = visual.ImageStim(
        win=win,
        name='image_square_trial', 
        image='C:/Users/Kyuchukova/Desktop/NOWASchool/choice_rtt/stimuli/shapes/response_square.jpg', mask=None, anchor='center',
        ori=0.0, pos=(0, -0.4), size=(0.2, 0.1),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=False, flipVert=False,
        texRes=128.0, interpolate=True, depth=-1.0)
    image_plus_trial = visual.ImageStim(
        win=win,
        name='image_plus_trial', 
        image='C:/Users/Kyuchukova/Desktop/NOWASchool/choice_rtt/stimuli/shapes/response_plus.jpg', mask=None, anchor='center',
        ori=0.0, pos=(0.25, -0.4), size=(0.2, 0.1),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=False, flipVert=False,
        texRes=128.0, interpolate=True, depth=-2.0)
    image_cross_trial = visual.ImageStim(
        win=win,
        name='image_cross_trial', 
        image='C:/Users/Kyuchukova/Desktop/NOWASchool/choice_rtt/stimuli/shapes/response_cross.jpg', mask=None, anchor='center',
        ori=0.0, pos=(-0.25, -0.4), size=(0.2, 0.1),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=False, flipVert=False,
        texRes=128.0, interpolate=True, depth=-3.0)
    left_far_tile = visual.ImageStim(
        win=win,
        name='left_far_tile', 
        image='C:/Users/Kyuchukova/Desktop/NOWASchool/choice_rtt/stimuli/shapes/white_square.png', mask=None, anchor='center',
        ori=0.0, pos=(-0.375, 0), size=(0.22, 0.22),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=False, flipVert=False,
        texRes=128.0, interpolate=True, depth=-4.0)
    left_mid_tile = visual.ImageStim(
        win=win,
        name='left_mid_tile', 
        image='C:/Users/Kyuchukova/Desktop/NOWASchool/choice_rtt/stimuli/shapes/white_square.png', mask=None, anchor='center',
        ori=0.0, pos=(-0.125, 0), size=(0.22, 0.22),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=False, flipVert=False,
        texRes=128.0, interpolate=True, depth=-5.0)
    right_mid_tile = visual.ImageStim(
        win=win,
        name='right_mid_tile', 
        image='C:/Users/Kyuchukova/Desktop/NOWASchool/choice_rtt/stimuli/shapes/white_square.png', mask=None, anchor='center',
        ori=0.0, pos=(0.125, 0), size=(0.22, 0.22),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=False, flipVert=False,
        texRes=128.0, interpolate=True, depth=-6.0)
    right_far_tile = visual.ImageStim(
        win=win,
        name='right_far_tile', 
        image='C:/Users/Kyuchukova/Desktop/NOWASchool/choice_rtt/stimuli/shapes/white_square.png', mask=None, anchor='center',
        ori=0.0, pos=(0.375, 0), size=(0.22, 0.22),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=False, flipVert=False,
        texRes=128.0, interpolate=True, depth=-7.0)
    target_image = visual.ImageStim(
        win=win,
        name='target_image', 
        image='default.png', mask=None, anchor='center',
        ori=0.0, pos=[0,0], size=(0.2, 0.2),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=False, flipVert=False,
        texRes=128.0, interpolate=True, depth=-8.0)
    trial_key = keyboard.Keyboard()
    
    # --- Initialize components for Routine "practice_feedback" ---
    feedback_text = visual.TextStim(win=win, name='feedback_text',
        text='',
        font='Open Sans',
        pos=(0, 0), height=0.05, wrapWidth=None, ori=0.0, 
        color='white', colorSpace='rgb', opacity=None, 
        languageStyle='LTR',
        depth=0.0);
    key_resp_practice = keyboard.Keyboard()
    
    # --- Initialize components for Routine "end_screen" ---
    text_end = visual.TextStim(win=win, name='text_end',
        text='You have reached the end of the experiment, thank you very much for participating. \n\nPlease press the space bar to finish.',
        font='Open Sans',
        pos=(0, 0), height=0.05, wrapWidth=None, ori=0.0, 
        color=[-1.0000, 1.0000, 1.0000], colorSpace='rgb', opacity=None, 
        languageStyle='LTR',
        depth=0.0);
    spacebar_end = keyboard.Keyboard()
    
    # create some handy timers
    if globalClock is None:
        globalClock = core.Clock()  # to track the time since experiment started
    if ioServer is not None:
        ioServer.syncClock(globalClock)
    logging.setDefaultClock(globalClock)
    routineTimer = core.Clock()  # to track time remaining of each (possibly non-slip) routine
    win.flip()  # flip window to reset last flip timer
    # store the exact time the global clock started
    expInfo['expStart'] = data.getDateStr(format='%Y-%m-%d %Hh%M.%S.%f %z', fractionalSecondDigits=6)
    
    # --- Prepare to start Routine "new_task" ---
    continueRoutine = True
    # update component parameters for each repeat
    thisExp.addData('new_task.started', globalClock.getTime())
    spacebar_introduction.keys = []
    spacebar_introduction.rt = []
    _spacebar_introduction_allKeys = []
    # Run 'Begin Routine' code from createDirectory
    # Construct the path with placeholders replaced by actual values
    dir_path = expInfo['output-path'] + "\sub-" + expInfo['Participant'] + "\ses-" + expInfo['Session']
    
    # Check if the path exists
    if not os.path.exists(dir_path):
        # Create the directory, including all intermediate directories
        os.makedirs(dir_path)
        print(f"Directory '{dir_path}' was created.")
    else:
        print(f"Directory '{dir_path}' already exists.")
    # keep track of which components have finished
    new_taskComponents = [introduction, spacebar_introduction]
    for thisComponent in new_taskComponents:
        thisComponent.tStart = None
        thisComponent.tStop = None
        thisComponent.tStartRefresh = None
        thisComponent.tStopRefresh = None
        if hasattr(thisComponent, 'status'):
            thisComponent.status = NOT_STARTED
    # reset timers
    t = 0
    _timeToFirstFrame = win.getFutureFlipTime(clock="now")
    frameN = -1
    
    # --- Run Routine "new_task" ---
    routineForceEnded = not continueRoutine
    while continueRoutine:
        # get current time
        t = routineTimer.getTime()
        tThisFlip = win.getFutureFlipTime(clock=routineTimer)
        tThisFlipGlobal = win.getFutureFlipTime(clock=None)
        frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
        # update/draw components on each frame
        
        # *introduction* updates
        
        # if introduction is starting this frame...
        if introduction.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            introduction.frameNStart = frameN  # exact frame index
            introduction.tStart = t  # local t and not account for scr refresh
            introduction.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(introduction, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'introduction.started')
            # update status
            introduction.status = STARTED
            introduction.setAutoDraw(True)
        
        # if introduction is active this frame...
        if introduction.status == STARTED:
            # update params
            pass
        
        # *spacebar_introduction* updates
        waitOnFlip = False
        
        # if spacebar_introduction is starting this frame...
        if spacebar_introduction.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            spacebar_introduction.frameNStart = frameN  # exact frame index
            spacebar_introduction.tStart = t  # local t and not account for scr refresh
            spacebar_introduction.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(spacebar_introduction, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'spacebar_introduction.started')
            # update status
            spacebar_introduction.status = STARTED
            # keyboard checking is just starting
            waitOnFlip = True
            win.callOnFlip(spacebar_introduction.clock.reset)  # t=0 on next screen flip
            win.callOnFlip(spacebar_introduction.clearEvents, eventType='keyboard')  # clear events on next screen flip
        if spacebar_introduction.status == STARTED and not waitOnFlip:
            theseKeys = spacebar_introduction.getKeys(keyList=['space'], ignoreKeys=["escape"], waitRelease=False)
            _spacebar_introduction_allKeys.extend(theseKeys)
            if len(_spacebar_introduction_allKeys):
                spacebar_introduction.keys = _spacebar_introduction_allKeys[-1].name  # just the last key pressed
                spacebar_introduction.rt = _spacebar_introduction_allKeys[-1].rt
                spacebar_introduction.duration = _spacebar_introduction_allKeys[-1].duration
                # a response ends the routine
                continueRoutine = False
        
        # check for quit (typically the Esc key)
        if defaultKeyboard.getKeys(keyList=["escape"]):
            thisExp.status = FINISHED
        if thisExp.status == FINISHED or endExpNow:
            endExperiment(thisExp, inputs=inputs, win=win)
            return
        
        # check if all components have finished
        if not continueRoutine:  # a component has requested a forced-end of Routine
            routineForceEnded = True
            break
        continueRoutine = False  # will revert to True if at least one component still running
        for thisComponent in new_taskComponents:
            if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                continueRoutine = True
                break  # at least one component has not yet finished
        
        # refresh the screen
        if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
            win.flip()
    
    # --- Ending Routine "new_task" ---
    for thisComponent in new_taskComponents:
        if hasattr(thisComponent, "setAutoDraw"):
            thisComponent.setAutoDraw(False)
    thisExp.addData('new_task.stopped', globalClock.getTime())
    # check responses
    if spacebar_introduction.keys in ['', [], None]:  # No response was made
        spacebar_introduction.keys = None
    thisExp.addData('spacebar_introduction.keys',spacebar_introduction.keys)
    if spacebar_introduction.keys != None:  # we had a response
        thisExp.addData('spacebar_introduction.rt', spacebar_introduction.rt)
        thisExp.addData('spacebar_introduction.duration', spacebar_introduction.duration)
    thisExp.nextEntry()
    # the Routine "new_task" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset()
    
    # --- Prepare to start Routine "instructions" ---
    continueRoutine = True
    # update component parameters for each repeat
    thisExp.addData('instructions.started', globalClock.getTime())
    spacebar_intro.keys = []
    spacebar_intro.rt = []
    _spacebar_intro_allKeys = []
    # keep track of which components have finished
    instructionsComponents = [text_instructions, spacebar_intro]
    for thisComponent in instructionsComponents:
        thisComponent.tStart = None
        thisComponent.tStop = None
        thisComponent.tStartRefresh = None
        thisComponent.tStopRefresh = None
        if hasattr(thisComponent, 'status'):
            thisComponent.status = NOT_STARTED
    # reset timers
    t = 0
    _timeToFirstFrame = win.getFutureFlipTime(clock="now")
    frameN = -1
    
    # --- Run Routine "instructions" ---
    routineForceEnded = not continueRoutine
    while continueRoutine:
        # get current time
        t = routineTimer.getTime()
        tThisFlip = win.getFutureFlipTime(clock=routineTimer)
        tThisFlipGlobal = win.getFutureFlipTime(clock=None)
        frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
        # update/draw components on each frame
        
        # *text_instructions* updates
        
        # if text_instructions is starting this frame...
        if text_instructions.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            text_instructions.frameNStart = frameN  # exact frame index
            text_instructions.tStart = t  # local t and not account for scr refresh
            text_instructions.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(text_instructions, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'text_instructions.started')
            # update status
            text_instructions.status = STARTED
            text_instructions.setAutoDraw(True)
        
        # if text_instructions is active this frame...
        if text_instructions.status == STARTED:
            # update params
            pass
        
        # *spacebar_intro* updates
        waitOnFlip = False
        
        # if spacebar_intro is starting this frame...
        if spacebar_intro.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            spacebar_intro.frameNStart = frameN  # exact frame index
            spacebar_intro.tStart = t  # local t and not account for scr refresh
            spacebar_intro.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(spacebar_intro, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'spacebar_intro.started')
            # update status
            spacebar_intro.status = STARTED
            # keyboard checking is just starting
            waitOnFlip = True
            win.callOnFlip(spacebar_intro.clock.reset)  # t=0 on next screen flip
            win.callOnFlip(spacebar_intro.clearEvents, eventType='keyboard')  # clear events on next screen flip
        if spacebar_intro.status == STARTED and not waitOnFlip:
            theseKeys = spacebar_intro.getKeys(keyList=['space'], ignoreKeys=["escape"], waitRelease=False)
            _spacebar_intro_allKeys.extend(theseKeys)
            if len(_spacebar_intro_allKeys):
                spacebar_intro.keys = _spacebar_intro_allKeys[-1].name  # just the last key pressed
                spacebar_intro.rt = _spacebar_intro_allKeys[-1].rt
                spacebar_intro.duration = _spacebar_intro_allKeys[-1].duration
                # a response ends the routine
                continueRoutine = False
        
        # check for quit (typically the Esc key)
        if defaultKeyboard.getKeys(keyList=["escape"]):
            thisExp.status = FINISHED
        if thisExp.status == FINISHED or endExpNow:
            endExperiment(thisExp, inputs=inputs, win=win)
            return
        
        # check if all components have finished
        if not continueRoutine:  # a component has requested a forced-end of Routine
            routineForceEnded = True
            break
        continueRoutine = False  # will revert to True if at least one component still running
        for thisComponent in instructionsComponents:
            if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                continueRoutine = True
                break  # at least one component has not yet finished
        
        # refresh the screen
        if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
            win.flip()
    
    # --- Ending Routine "instructions" ---
    for thisComponent in instructionsComponents:
        if hasattr(thisComponent, "setAutoDraw"):
            thisComponent.setAutoDraw(False)
    thisExp.addData('instructions.stopped', globalClock.getTime())
    # check responses
    if spacebar_intro.keys in ['', [], None]:  # No response was made
        spacebar_intro.keys = None
    thisExp.addData('spacebar_intro.keys',spacebar_intro.keys)
    if spacebar_intro.keys != None:  # we had a response
        thisExp.addData('spacebar_intro.rt', spacebar_intro.rt)
        thisExp.addData('spacebar_intro.duration', spacebar_intro.duration)
    thisExp.nextEntry()
    # the Routine "instructions" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset()
    
    # --- Prepare to start Routine "instruct_shape" ---
    continueRoutine = True
    # update component parameters for each repeat
    thisExp.addData('instruct_shape.started', globalClock.getTime())
    instruct_space_bar.keys = []
    instruct_space_bar.rt = []
    _instruct_space_bar_allKeys = []
    # keep track of which components have finished
    instruct_shapeComponents = [instructions_shape, instruct_space_bar, image_square, image_plus, image_cross]
    for thisComponent in instruct_shapeComponents:
        thisComponent.tStart = None
        thisComponent.tStop = None
        thisComponent.tStartRefresh = None
        thisComponent.tStopRefresh = None
        if hasattr(thisComponent, 'status'):
            thisComponent.status = NOT_STARTED
    # reset timers
    t = 0
    _timeToFirstFrame = win.getFutureFlipTime(clock="now")
    frameN = -1
    
    # --- Run Routine "instruct_shape" ---
    routineForceEnded = not continueRoutine
    while continueRoutine:
        # get current time
        t = routineTimer.getTime()
        tThisFlip = win.getFutureFlipTime(clock=routineTimer)
        tThisFlipGlobal = win.getFutureFlipTime(clock=None)
        frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
        # update/draw components on each frame
        
        # *instructions_shape* updates
        
        # if instructions_shape is starting this frame...
        if instructions_shape.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            instructions_shape.frameNStart = frameN  # exact frame index
            instructions_shape.tStart = t  # local t and not account for scr refresh
            instructions_shape.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(instructions_shape, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'instructions_shape.started')
            # update status
            instructions_shape.status = STARTED
            instructions_shape.setAutoDraw(True)
        
        # if instructions_shape is active this frame...
        if instructions_shape.status == STARTED:
            # update params
            pass
        
        # *instruct_space_bar* updates
        waitOnFlip = False
        
        # if instruct_space_bar is starting this frame...
        if instruct_space_bar.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            instruct_space_bar.frameNStart = frameN  # exact frame index
            instruct_space_bar.tStart = t  # local t and not account for scr refresh
            instruct_space_bar.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(instruct_space_bar, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'instruct_space_bar.started')
            # update status
            instruct_space_bar.status = STARTED
            # keyboard checking is just starting
            waitOnFlip = True
            win.callOnFlip(instruct_space_bar.clock.reset)  # t=0 on next screen flip
            win.callOnFlip(instruct_space_bar.clearEvents, eventType='keyboard')  # clear events on next screen flip
        if instruct_space_bar.status == STARTED and not waitOnFlip:
            theseKeys = instruct_space_bar.getKeys(keyList=['space'], ignoreKeys=["escape"], waitRelease=False)
            _instruct_space_bar_allKeys.extend(theseKeys)
            if len(_instruct_space_bar_allKeys):
                instruct_space_bar.keys = _instruct_space_bar_allKeys[-1].name  # just the last key pressed
                instruct_space_bar.rt = _instruct_space_bar_allKeys[-1].rt
                instruct_space_bar.duration = _instruct_space_bar_allKeys[-1].duration
                # a response ends the routine
                continueRoutine = False
        
        # *image_square* updates
        
        # if image_square is starting this frame...
        if image_square.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            image_square.frameNStart = frameN  # exact frame index
            image_square.tStart = t  # local t and not account for scr refresh
            image_square.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(image_square, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'image_square.started')
            # update status
            image_square.status = STARTED
            image_square.setAutoDraw(True)
        
        # if image_square is active this frame...
        if image_square.status == STARTED:
            # update params
            pass
        
        # *image_plus* updates
        
        # if image_plus is starting this frame...
        if image_plus.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            image_plus.frameNStart = frameN  # exact frame index
            image_plus.tStart = t  # local t and not account for scr refresh
            image_plus.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(image_plus, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'image_plus.started')
            # update status
            image_plus.status = STARTED
            image_plus.setAutoDraw(True)
        
        # if image_plus is active this frame...
        if image_plus.status == STARTED:
            # update params
            pass
        
        # *image_cross* updates
        
        # if image_cross is starting this frame...
        if image_cross.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            image_cross.frameNStart = frameN  # exact frame index
            image_cross.tStart = t  # local t and not account for scr refresh
            image_cross.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(image_cross, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'image_cross.started')
            # update status
            image_cross.status = STARTED
            image_cross.setAutoDraw(True)
        
        # if image_cross is active this frame...
        if image_cross.status == STARTED:
            # update params
            pass
        
        # check for quit (typically the Esc key)
        if defaultKeyboard.getKeys(keyList=["escape"]):
            thisExp.status = FINISHED
        if thisExp.status == FINISHED or endExpNow:
            endExperiment(thisExp, inputs=inputs, win=win)
            return
        
        # check if all components have finished
        if not continueRoutine:  # a component has requested a forced-end of Routine
            routineForceEnded = True
            break
        continueRoutine = False  # will revert to True if at least one component still running
        for thisComponent in instruct_shapeComponents:
            if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                continueRoutine = True
                break  # at least one component has not yet finished
        
        # refresh the screen
        if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
            win.flip()
    
    # --- Ending Routine "instruct_shape" ---
    for thisComponent in instruct_shapeComponents:
        if hasattr(thisComponent, "setAutoDraw"):
            thisComponent.setAutoDraw(False)
    thisExp.addData('instruct_shape.stopped', globalClock.getTime())
    # check responses
    if instruct_space_bar.keys in ['', [], None]:  # No response was made
        instruct_space_bar.keys = None
    thisExp.addData('instruct_space_bar.keys',instruct_space_bar.keys)
    if instruct_space_bar.keys != None:  # we had a response
        thisExp.addData('instruct_space_bar.rt', instruct_space_bar.rt)
        thisExp.addData('instruct_space_bar.duration', instruct_space_bar.duration)
    thisExp.nextEntry()
    # the Routine "instruct_shape" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset()
    
    # set up handler to look after randomisation of conditions etc
    trials_loop = data.TrialHandler(nReps=1.0, method='random', 
        extraInfo=expInfo, originPath=-1,
        trialList=data.importConditions('shapes_targets.csv'),
        seed=None, name='trials_loop')
    thisExp.addLoop(trials_loop)  # add the loop to the experiment
    thisTrials_loop = trials_loop.trialList[0]  # so we can initialise stimuli with some values
    # abbreviate parameter names if possible (e.g. rgb = thisTrials_loop.rgb)
    if thisTrials_loop != None:
        for paramName in thisTrials_loop:
            globals()[paramName] = thisTrials_loop[paramName]
    
    for thisTrials_loop in trials_loop:
        currentLoop = trials_loop
        thisExp.timestampOnFlip(win, 'thisRow.t')
        # pause experiment here if requested
        if thisExp.status == PAUSED:
            pauseExperiment(
                thisExp=thisExp, 
                inputs=inputs, 
                win=win, 
                timers=[routineTimer], 
                playbackComponents=[]
        )
        # abbreviate parameter names if possible (e.g. rgb = thisTrials_loop.rgb)
        if thisTrials_loop != None:
            for paramName in thisTrials_loop:
                globals()[paramName] = thisTrials_loop[paramName]
        
        # --- Prepare to start Routine "trial" ---
        continueRoutine = True
        # update component parameters for each repeat
        thisExp.addData('trial.started', globalClock.getTime())
        # Run 'Begin Routine' code from jiterOnsetPosition
        #list of possible onsets for target
        list_onsets = [1, 1.2, 1.4, 1.6, 1.8]
        
        # randomize these onsets
        shuffle(list_onsets)
        
        #pick the first value from the list 
        onset_trial = list_onsets[0]
        
        #list of possible positions for target
        list_positions = [-0.375, -0.125, 0.125, 0.375]
        
        # randomize these onsets
        shuffle(list_positions)
        
        #pick the first value from the list 
        position_trial = list_positions[0]
        
        #TargetImage is the variable in the Image field of the target_image component
        
        #path of where to find the target image
        if TargetImage == '../../stimuli/shapes/target_square.jpg': #path of where to find the target image
            
            #setting the key press that will be the correct answer
            corrAns = 'v' 
            
        #path of where to find the target image
        elif TargetImage == '../../stimuli/shapes/target_cross.jpg':
        
            #setting the key press that will be the correct answer
            corrAns = 'c'
        
        #path of where to find the target image
        elif TargetImage == '../../stimuli/shapes/target_plus.jpg':
        
            #setting the key press that will be the correct answer
            corrAns = 'b'
        target_image.setPos((position_trial, 0))
        target_image.setImage(TargetImage)
        trial_key.keys = []
        trial_key.rt = []
        _trial_key_allKeys = []
        # keep track of which components have finished
        trialComponents = [image_square_trial, image_plus_trial, image_cross_trial, left_far_tile, left_mid_tile, right_mid_tile, right_far_tile, target_image, trial_key]
        for thisComponent in trialComponents:
            thisComponent.tStart = None
            thisComponent.tStop = None
            thisComponent.tStartRefresh = None
            thisComponent.tStopRefresh = None
            if hasattr(thisComponent, 'status'):
                thisComponent.status = NOT_STARTED
        # reset timers
        t = 0
        _timeToFirstFrame = win.getFutureFlipTime(clock="now")
        frameN = -1
        
        # --- Run Routine "trial" ---
        routineForceEnded = not continueRoutine
        while continueRoutine:
            # get current time
            t = routineTimer.getTime()
            tThisFlip = win.getFutureFlipTime(clock=routineTimer)
            tThisFlipGlobal = win.getFutureFlipTime(clock=None)
            frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
            # update/draw components on each frame
            
            # *image_square_trial* updates
            
            # if image_square_trial is starting this frame...
            if image_square_trial.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
                # keep track of start time/frame for later
                image_square_trial.frameNStart = frameN  # exact frame index
                image_square_trial.tStart = t  # local t and not account for scr refresh
                image_square_trial.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(image_square_trial, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'image_square_trial.started')
                # update status
                image_square_trial.status = STARTED
                image_square_trial.setAutoDraw(True)
            
            # if image_square_trial is active this frame...
            if image_square_trial.status == STARTED:
                # update params
                pass
            
            # *image_plus_trial* updates
            
            # if image_plus_trial is starting this frame...
            if image_plus_trial.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
                # keep track of start time/frame for later
                image_plus_trial.frameNStart = frameN  # exact frame index
                image_plus_trial.tStart = t  # local t and not account for scr refresh
                image_plus_trial.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(image_plus_trial, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'image_plus_trial.started')
                # update status
                image_plus_trial.status = STARTED
                image_plus_trial.setAutoDraw(True)
            
            # if image_plus_trial is active this frame...
            if image_plus_trial.status == STARTED:
                # update params
                pass
            
            # *image_cross_trial* updates
            
            # if image_cross_trial is starting this frame...
            if image_cross_trial.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
                # keep track of start time/frame for later
                image_cross_trial.frameNStart = frameN  # exact frame index
                image_cross_trial.tStart = t  # local t and not account for scr refresh
                image_cross_trial.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(image_cross_trial, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'image_cross_trial.started')
                # update status
                image_cross_trial.status = STARTED
                image_cross_trial.setAutoDraw(True)
            
            # if image_cross_trial is active this frame...
            if image_cross_trial.status == STARTED:
                # update params
                pass
            
            # *left_far_tile* updates
            
            # if left_far_tile is starting this frame...
            if left_far_tile.status == NOT_STARTED and tThisFlip >= 0.5-frameTolerance:
                # keep track of start time/frame for later
                left_far_tile.frameNStart = frameN  # exact frame index
                left_far_tile.tStart = t  # local t and not account for scr refresh
                left_far_tile.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(left_far_tile, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'left_far_tile.started')
                # update status
                left_far_tile.status = STARTED
                left_far_tile.setAutoDraw(True)
            
            # if left_far_tile is active this frame...
            if left_far_tile.status == STARTED:
                # update params
                pass
            
            # *left_mid_tile* updates
            
            # if left_mid_tile is starting this frame...
            if left_mid_tile.status == NOT_STARTED and tThisFlip >= 0.5-frameTolerance:
                # keep track of start time/frame for later
                left_mid_tile.frameNStart = frameN  # exact frame index
                left_mid_tile.tStart = t  # local t and not account for scr refresh
                left_mid_tile.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(left_mid_tile, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'left_mid_tile.started')
                # update status
                left_mid_tile.status = STARTED
                left_mid_tile.setAutoDraw(True)
            
            # if left_mid_tile is active this frame...
            if left_mid_tile.status == STARTED:
                # update params
                pass
            
            # *right_mid_tile* updates
            
            # if right_mid_tile is starting this frame...
            if right_mid_tile.status == NOT_STARTED and tThisFlip >= 0.5-frameTolerance:
                # keep track of start time/frame for later
                right_mid_tile.frameNStart = frameN  # exact frame index
                right_mid_tile.tStart = t  # local t and not account for scr refresh
                right_mid_tile.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(right_mid_tile, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'right_mid_tile.started')
                # update status
                right_mid_tile.status = STARTED
                right_mid_tile.setAutoDraw(True)
            
            # if right_mid_tile is active this frame...
            if right_mid_tile.status == STARTED:
                # update params
                pass
            
            # *right_far_tile* updates
            
            # if right_far_tile is starting this frame...
            if right_far_tile.status == NOT_STARTED and tThisFlip >= 0.5-frameTolerance:
                # keep track of start time/frame for later
                right_far_tile.frameNStart = frameN  # exact frame index
                right_far_tile.tStart = t  # local t and not account for scr refresh
                right_far_tile.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(right_far_tile, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'right_far_tile.started')
                # update status
                right_far_tile.status = STARTED
                right_far_tile.setAutoDraw(True)
            
            # if right_far_tile is active this frame...
            if right_far_tile.status == STARTED:
                # update params
                pass
            
            # *target_image* updates
            
            # if target_image is starting this frame...
            if target_image.status == NOT_STARTED and tThisFlip >= onset_trial-frameTolerance:
                # keep track of start time/frame for later
                target_image.frameNStart = frameN  # exact frame index
                target_image.tStart = t  # local t and not account for scr refresh
                target_image.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(target_image, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'target_image.started')
                # update status
                target_image.status = STARTED
                target_image.setAutoDraw(True)
            
            # if target_image is active this frame...
            if target_image.status == STARTED:
                # update params
                pass
            
            # if target_image is stopping this frame...
            if target_image.status == STARTED:
                # is it time to stop? (based on global clock, using actual start)
                if tThisFlipGlobal > target_image.tStartRefresh + 0.2-frameTolerance:
                    # keep track of stop time/frame for later
                    target_image.tStop = t  # not accounting for scr refresh
                    target_image.frameNStop = frameN  # exact frame index
                    # add timestamp to datafile
                    thisExp.timestampOnFlip(win, 'target_image.stopped')
                    # update status
                    target_image.status = FINISHED
                    target_image.setAutoDraw(False)
            
            # *trial_key* updates
            
            # if trial_key is starting this frame...
            if trial_key.status == NOT_STARTED and t >= 0.0-frameTolerance:
                # keep track of start time/frame for later
                trial_key.frameNStart = frameN  # exact frame index
                trial_key.tStart = t  # local t and not account for scr refresh
                trial_key.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(trial_key, 'tStartRefresh')  # time at next scr refresh
                # update status
                trial_key.status = STARTED
                # keyboard checking is just starting
                trial_key.clock.reset()  # now t=0
                trial_key.clearEvents(eventType='keyboard')
            if trial_key.status == STARTED:
                theseKeys = trial_key.getKeys(keyList=['c','v','b'], ignoreKeys=["escape"], waitRelease=False)
                _trial_key_allKeys.extend(theseKeys)
                if len(_trial_key_allKeys):
                    trial_key.keys = _trial_key_allKeys[-1].name  # just the last key pressed
                    trial_key.rt = _trial_key_allKeys[-1].rt
                    trial_key.duration = _trial_key_allKeys[-1].duration
                    # was this correct?
                    if (trial_key.keys == str(corrAns)) or (trial_key.keys == corrAns):
                        trial_key.corr = 1
                    else:
                        trial_key.corr = 0
                    # a response ends the routine
                    continueRoutine = False
            
            # check for quit (typically the Esc key)
            if defaultKeyboard.getKeys(keyList=["escape"]):
                thisExp.status = FINISHED
            if thisExp.status == FINISHED or endExpNow:
                endExperiment(thisExp, inputs=inputs, win=win)
                return
            
            # check if all components have finished
            if not continueRoutine:  # a component has requested a forced-end of Routine
                routineForceEnded = True
                break
            continueRoutine = False  # will revert to True if at least one component still running
            for thisComponent in trialComponents:
                if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                    continueRoutine = True
                    break  # at least one component has not yet finished
            
            # refresh the screen
            if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
                win.flip()
        
        # --- Ending Routine "trial" ---
        for thisComponent in trialComponents:
            if hasattr(thisComponent, "setAutoDraw"):
                thisComponent.setAutoDraw(False)
        thisExp.addData('trial.stopped', globalClock.getTime())
        # check responses
        if trial_key.keys in ['', [], None]:  # No response was made
            trial_key.keys = None
            # was no response the correct answer?!
            if str(corrAns).lower() == 'none':
               trial_key.corr = 1;  # correct non-response
            else:
               trial_key.corr = 0;  # failed to respond (incorrectly)
        # store data for trials_loop (TrialHandler)
        trials_loop.addData('trial_key.keys',trial_key.keys)
        trials_loop.addData('trial_key.corr', trial_key.corr)
        if trial_key.keys != None:  # we had a response
            trials_loop.addData('trial_key.rt', trial_key.rt)
            trials_loop.addData('trial_key.duration', trial_key.duration)
        # Run 'End Routine' code from compRTandAcc
        #this code is to record the reaction times and accuracy of the trial
        
        thisRoutineDuration = t # how long did this trial last
        
        # keyboard_response.rt is the time with which a key was pressed
        # thisRecRT - how long it took for participants to respond after onset of target_image
        # thisAcc - whether or not the response was correct
        
        # compute RT based on onset of target
        thisRecRT = trial_key.rt - onset_trial 
        
        # check of the response was correct
        if trial_key.corr == 1: 
        
            # if it was correct, assign 'correct' value
            thisAcc = 'correct' 
        
        # if not, assign 'incorrect' value
        else:
            thisAcc = 'incorrect'
        
        # record the actual response times of each trial 
        thisExp.addData('trialRespTimes', thisRecRT) 
        
        # the Routine "trial" was not non-slip safe, so reset the non-slip timer
        routineTimer.reset()
        
        # --- Prepare to start Routine "practice_feedback" ---
        continueRoutine = True
        # update component parameters for each repeat
        thisExp.addData('practice_feedback.started', globalClock.getTime())
        feedback_text.setText("The last recorded RT was: "  + str(round(thisRecRT, 3)) + " \nThe response was: "  + thisAcc + " \n\nPress the space bar to continue."  
        )
        key_resp_practice.keys = []
        key_resp_practice.rt = []
        _key_resp_practice_allKeys = []
        # keep track of which components have finished
        practice_feedbackComponents = [feedback_text, key_resp_practice]
        for thisComponent in practice_feedbackComponents:
            thisComponent.tStart = None
            thisComponent.tStop = None
            thisComponent.tStartRefresh = None
            thisComponent.tStopRefresh = None
            if hasattr(thisComponent, 'status'):
                thisComponent.status = NOT_STARTED
        # reset timers
        t = 0
        _timeToFirstFrame = win.getFutureFlipTime(clock="now")
        frameN = -1
        
        # --- Run Routine "practice_feedback" ---
        routineForceEnded = not continueRoutine
        while continueRoutine:
            # get current time
            t = routineTimer.getTime()
            tThisFlip = win.getFutureFlipTime(clock=routineTimer)
            tThisFlipGlobal = win.getFutureFlipTime(clock=None)
            frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
            # update/draw components on each frame
            
            # *feedback_text* updates
            
            # if feedback_text is starting this frame...
            if feedback_text.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
                # keep track of start time/frame for later
                feedback_text.frameNStart = frameN  # exact frame index
                feedback_text.tStart = t  # local t and not account for scr refresh
                feedback_text.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(feedback_text, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'feedback_text.started')
                # update status
                feedback_text.status = STARTED
                feedback_text.setAutoDraw(True)
            
            # if feedback_text is active this frame...
            if feedback_text.status == STARTED:
                # update params
                pass
            
            # *key_resp_practice* updates
            waitOnFlip = False
            
            # if key_resp_practice is starting this frame...
            if key_resp_practice.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
                # keep track of start time/frame for later
                key_resp_practice.frameNStart = frameN  # exact frame index
                key_resp_practice.tStart = t  # local t and not account for scr refresh
                key_resp_practice.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(key_resp_practice, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'key_resp_practice.started')
                # update status
                key_resp_practice.status = STARTED
                # keyboard checking is just starting
                waitOnFlip = True
                win.callOnFlip(key_resp_practice.clock.reset)  # t=0 on next screen flip
                win.callOnFlip(key_resp_practice.clearEvents, eventType='keyboard')  # clear events on next screen flip
            if key_resp_practice.status == STARTED and not waitOnFlip:
                theseKeys = key_resp_practice.getKeys(keyList=['space'], ignoreKeys=["escape"], waitRelease=False)
                _key_resp_practice_allKeys.extend(theseKeys)
                if len(_key_resp_practice_allKeys):
                    key_resp_practice.keys = _key_resp_practice_allKeys[-1].name  # just the last key pressed
                    key_resp_practice.rt = _key_resp_practice_allKeys[-1].rt
                    key_resp_practice.duration = _key_resp_practice_allKeys[-1].duration
                    # a response ends the routine
                    continueRoutine = False
            
            # check for quit (typically the Esc key)
            if defaultKeyboard.getKeys(keyList=["escape"]):
                thisExp.status = FINISHED
            if thisExp.status == FINISHED or endExpNow:
                endExperiment(thisExp, inputs=inputs, win=win)
                return
            
            # check if all components have finished
            if not continueRoutine:  # a component has requested a forced-end of Routine
                routineForceEnded = True
                break
            continueRoutine = False  # will revert to True if at least one component still running
            for thisComponent in practice_feedbackComponents:
                if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                    continueRoutine = True
                    break  # at least one component has not yet finished
            
            # refresh the screen
            if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
                win.flip()
        
        # --- Ending Routine "practice_feedback" ---
        for thisComponent in practice_feedbackComponents:
            if hasattr(thisComponent, "setAutoDraw"):
                thisComponent.setAutoDraw(False)
        thisExp.addData('practice_feedback.stopped', globalClock.getTime())
        # check responses
        if key_resp_practice.keys in ['', [], None]:  # No response was made
            key_resp_practice.keys = None
        trials_loop.addData('key_resp_practice.keys',key_resp_practice.keys)
        if key_resp_practice.keys != None:  # we had a response
            trials_loop.addData('key_resp_practice.rt', key_resp_practice.rt)
            trials_loop.addData('key_resp_practice.duration', key_resp_practice.duration)
        # the Routine "practice_feedback" was not non-slip safe, so reset the non-slip timer
        routineTimer.reset()
        thisExp.nextEntry()
        
        if thisSession is not None:
            # if running in a Session with a Liaison client, send data up to now
            thisSession.sendExperimentData()
    # completed 1.0 repeats of 'trials_loop'
    
    
    # --- Prepare to start Routine "end_screen" ---
    continueRoutine = True
    # update component parameters for each repeat
    thisExp.addData('end_screen.started', globalClock.getTime())
    spacebar_end.keys = []
    spacebar_end.rt = []
    _spacebar_end_allKeys = []
    # keep track of which components have finished
    end_screenComponents = [text_end, spacebar_end]
    for thisComponent in end_screenComponents:
        thisComponent.tStart = None
        thisComponent.tStop = None
        thisComponent.tStartRefresh = None
        thisComponent.tStopRefresh = None
        if hasattr(thisComponent, 'status'):
            thisComponent.status = NOT_STARTED
    # reset timers
    t = 0
    _timeToFirstFrame = win.getFutureFlipTime(clock="now")
    frameN = -1
    
    # --- Run Routine "end_screen" ---
    routineForceEnded = not continueRoutine
    while continueRoutine:
        # get current time
        t = routineTimer.getTime()
        tThisFlip = win.getFutureFlipTime(clock=routineTimer)
        tThisFlipGlobal = win.getFutureFlipTime(clock=None)
        frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
        # update/draw components on each frame
        
        # *text_end* updates
        
        # if text_end is starting this frame...
        if text_end.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            text_end.frameNStart = frameN  # exact frame index
            text_end.tStart = t  # local t and not account for scr refresh
            text_end.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(text_end, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'text_end.started')
            # update status
            text_end.status = STARTED
            text_end.setAutoDraw(True)
        
        # if text_end is active this frame...
        if text_end.status == STARTED:
            # update params
            pass
        
        # *spacebar_end* updates
        waitOnFlip = False
        
        # if spacebar_end is starting this frame...
        if spacebar_end.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            spacebar_end.frameNStart = frameN  # exact frame index
            spacebar_end.tStart = t  # local t and not account for scr refresh
            spacebar_end.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(spacebar_end, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'spacebar_end.started')
            # update status
            spacebar_end.status = STARTED
            # keyboard checking is just starting
            waitOnFlip = True
            win.callOnFlip(spacebar_end.clock.reset)  # t=0 on next screen flip
            win.callOnFlip(spacebar_end.clearEvents, eventType='keyboard')  # clear events on next screen flip
        if spacebar_end.status == STARTED and not waitOnFlip:
            theseKeys = spacebar_end.getKeys(keyList=['space'], ignoreKeys=["escape"], waitRelease=False)
            _spacebar_end_allKeys.extend(theseKeys)
            if len(_spacebar_end_allKeys):
                spacebar_end.keys = _spacebar_end_allKeys[-1].name  # just the last key pressed
                spacebar_end.rt = _spacebar_end_allKeys[-1].rt
                spacebar_end.duration = _spacebar_end_allKeys[-1].duration
                # a response ends the routine
                continueRoutine = False
        
        # check for quit (typically the Esc key)
        if defaultKeyboard.getKeys(keyList=["escape"]):
            thisExp.status = FINISHED
        if thisExp.status == FINISHED or endExpNow:
            endExperiment(thisExp, inputs=inputs, win=win)
            return
        
        # check if all components have finished
        if not continueRoutine:  # a component has requested a forced-end of Routine
            routineForceEnded = True
            break
        continueRoutine = False  # will revert to True if at least one component still running
        for thisComponent in end_screenComponents:
            if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                continueRoutine = True
                break  # at least one component has not yet finished
        
        # refresh the screen
        if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
            win.flip()
    
    # --- Ending Routine "end_screen" ---
    for thisComponent in end_screenComponents:
        if hasattr(thisComponent, "setAutoDraw"):
            thisComponent.setAutoDraw(False)
    thisExp.addData('end_screen.stopped', globalClock.getTime())
    # check responses
    if spacebar_end.keys in ['', [], None]:  # No response was made
        spacebar_end.keys = None
    thisExp.addData('spacebar_end.keys',spacebar_end.keys)
    if spacebar_end.keys != None:  # we had a response
        thisExp.addData('spacebar_end.rt', spacebar_end.rt)
        thisExp.addData('spacebar_end.duration', spacebar_end.duration)
    thisExp.nextEntry()
    # the Routine "end_screen" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset()
    
    # mark experiment as finished
    endExperiment(thisExp, win=win, inputs=inputs)


def saveData(thisExp):
    """
    Save data from this experiment
    
    Parameters
    ==========
    thisExp : psychopy.data.ExperimentHandler
        Handler object for this experiment, contains the data to save and information about 
        where to save it to.
    """
    filename = thisExp.dataFileName
    # these shouldn't be strictly necessary (should auto-save)
    thisExp.saveAsWideText(filename + '.csv', delim='auto')
    thisExp.saveAsPickle(filename)


def endExperiment(thisExp, inputs=None, win=None):
    """
    End this experiment, performing final shut down operations.
    
    This function does NOT close the window or end the Python process - use `quit` for this.
    
    Parameters
    ==========
    thisExp : psychopy.data.ExperimentHandler
        Handler object for this experiment, contains the data to save and information about 
        where to save it to.
    inputs : dict
        Dictionary of input devices by name.
    win : psychopy.visual.Window
        Window for this experiment.
    """
    if win is not None:
        # remove autodraw from all current components
        win.clearAutoDraw()
        # Flip one final time so any remaining win.callOnFlip() 
        # and win.timeOnFlip() tasks get executed
        win.flip()
    # mark experiment handler as finished
    thisExp.status = FINISHED
    # shut down eyetracker, if there is one
    if inputs is not None:
        if 'eyetracker' in inputs and inputs['eyetracker'] is not None:
            inputs['eyetracker'].setConnectionState(False)
    logging.flush()


def quit(thisExp, win=None, inputs=None, thisSession=None):
    """
    Fully quit, closing the window and ending the Python process.
    
    Parameters
    ==========
    win : psychopy.visual.Window
        Window to close.
    inputs : dict
        Dictionary of input devices by name.
    thisSession : psychopy.session.Session or None
        Handle of the Session object this experiment is being run from, if any.
    """
    thisExp.abort()  # or data files will save again on exit
    # make sure everything is closed down
    if win is not None:
        # Flip one final time so any remaining win.callOnFlip() 
        # and win.timeOnFlip() tasks get executed before quitting
        win.flip()
        win.close()
    if inputs is not None:
        if 'eyetracker' in inputs and inputs['eyetracker'] is not None:
            inputs['eyetracker'].setConnectionState(False)
    logging.flush()
    if thisSession is not None:
        thisSession.stop()
    # terminate Python process
    core.quit()


# if running this experiment as a script...
if __name__ == '__main__':
    # call all functions in order
    expInfo = showExpInfoDlg(expInfo=expInfo)
    thisExp = setupData(expInfo=expInfo)
    logFile = setupLogging(filename=thisExp.dataFileName)
    win = setupWindow(expInfo=expInfo)
    inputs = setupInputs(expInfo=expInfo, thisExp=thisExp, win=win)
    run(
        expInfo=expInfo, 
        thisExp=thisExp, 
        win=win, 
        inputs=inputs
    )
    saveData(thisExp=thisExp)
    quit(thisExp=thisExp, win=win, inputs=inputs)
